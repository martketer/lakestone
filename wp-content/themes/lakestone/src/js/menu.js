/* globals jQuery, $ */

jQuery(function ($) {
  $('.nav-main').on('click', function (e) {
    if ($(window).innerWidth() <= 768) {
      if (!$('nav').hasClass('active')) {
        $(this).find('ul').fadeIn()
        $(this).addClass('active')
      } else if ($('nav').hasClass('active')) {
        $(this).find('ul').fadeOut()
        $(this).removeClass('active')
      }
    }
  })

	// build scenes
  if($('body').hasClass('home')){


    $(".header").headroom({
      offset: $('.hero').offset().top
    });

    // init controller
  	var menuController = new ScrollMagic.Controller()


    var elTop = jQuery('.header .menu-item a[href="/#top"]').parent()
    var scene = new ScrollMagic.Scene({triggerElement: "#top"})
            .on("enter", function(e) {
              updateIndicator(elTop)
            })
  					.setClassToggle(elTop[0], "in-view")
            .duration(
              jQuery('.hero').outerHeight()
            )
  					.addTo(menuController);
    var elFeatures = jQuery('.header .menu-item a[href="/#features"]').parent()
  	var scene = new ScrollMagic.Scene({triggerElement: "#features"})
            .on("enter", function() {
              updateIndicator(elFeatures)
            })
  					.setClassToggle(elFeatures[0], "in-view")
            .duration(
              jQuery('.features').outerHeight()
            )
  					.addTo(menuController);
    var elLifestyle = jQuery('.header .menu-item a[href="/#lifestyle"]').parent()
  	var scene = new ScrollMagic.Scene({triggerElement: "#lifestyle"})
            .on("enter", function() {
              updateIndicator(elLifestyle)
            })
  					.setClassToggle(elLifestyle[0], "in-view")
            .duration(
              jQuery('.highlight').outerHeight()
            )
  					.addTo(menuController);
    var elDeveloper = jQuery('.header .menu-item a[href="/#developer"]').parent()
    var scene = new ScrollMagic.Scene({triggerElement: "#developer"})
            .on("enter", function() {
              updateIndicator(elDeveloper)
            })
  					.setClassToggle(elDeveloper[0], "in-view")
            .duration(
              jQuery('.about').outerHeight()
            )
  					.addTo(menuController);
    var elRegister = jQuery('.header .menu-item a[href="/#register"]').parent()
    var scene = new ScrollMagic.Scene({triggerElement: "#register"})
            .on("enter", function() {
              updateIndicator(elRegister)
            })
  					.setClassToggle(elRegister[0], "in-view")
            .duration(
              jQuery('.register').outerHeight()
            )
  					.addTo(menuController);
    var elContact = jQuery('.header .menu-item a[href="#contact"]').parent()
    var scene = new ScrollMagic.Scene({triggerElement: "#contact"})
            .on("enter", function() {
              updateIndicator(elContact)
            })
  					.setClassToggle(elContact[0], "in-view")
            .duration(
              jQuery('.footer').outerHeight()
            )
  					.addTo(menuController);
  }

  //indicator
  function updateIndicator(element) {
    console.log(typeof(element))
    var $e = element.find('a')
    var center = $e.offset().left + ($e.width() / 2)
    center -= $('.header .nav-main').offset().left
    var $indicator = $('.header .indicator')
    $indicator.css('left', center)
  }

  // smooth scrolling
  // Select all links with hashes
  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 100
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
})
