/* global jQuery */

jQuery(function ($) {
  $('body').addClass('loaded')

  //init controller
  var controller = new ScrollMagic.Controller();

  // build tween
  var current = $('.bg-image').css('background-size')
  var target

  if ($(window).outerHeight > 1600) {
    target = current.replace('100', '130')
  } else {
    target = current.replace('100', '120')
  }
	var tween = TweenMax.to("#bg", 1, {
    "background-size": target,
    "background-position-x": "20%"
  })

  // Intro animations
  new ScrollMagic.Scene({triggerElement: "#graphic", duration: $('.hero').outerHeight()})
		.setPin("#bg")
    .setTween(tween)
    .triggerHook(0)
		.addTo(controller)
  new ScrollMagic.Scene({triggerElement: "#top", "duration": "100%"})
    .setClassToggle($('.hero .hero__heading')[0], 'text-anim')
    .triggerHook(0.2)
    .addTo(controller)
  new ScrollMagic.Scene({triggerElement: "#lifestyle", "duration": "80%"})
    .setClassToggle($('.highlight .highlight__heading')[0], 'text-anim')
    .triggerHook(.3)
    .addTo(controller)

  $('.text-hidden').each(function() {
    var delay = 0
    $(this).find('.line').each(function() {
      $(this).css({
        'transition-delay': delay+'s'
      })
      delay += 0.1
    })
  })

})

function initMap() {
  var location = {lat: 49.500655, lng: -119.592645}
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: location,
    disableDefaultUI: true,
    styles: [{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}]
  })
  var icon = {
  url: '/wp-content/themes/lakestone/dist/img/map-marker.png',
  size: new google.maps.Size(96, 96),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(12, 12),
  scaledSize: new google.maps.Size(24, 24)
}
var marker = new google.maps.Marker({
  position: location,
  map: map,
  icon: icon
})
}
